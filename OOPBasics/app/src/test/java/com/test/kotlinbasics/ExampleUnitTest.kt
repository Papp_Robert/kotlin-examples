package com.test.kotlinbasics

import android.util.Log
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }


    @Test
    fun valueType() {
        val i = 150

        println(i)

        changeValueType(i)

        println(i)
    }

    private fun changeValueType(value: Int) {
        var xI: Int = value
        xI = 250

        println(xI)
    }


    @Test
    fun referenceType() {
        val person = Person("István")

        println(person)

        changeReferenceTypeValue(person)

        println(person)
    }

    private fun changeReferenceTypeValue(person: Person) {
        person.name = "Robi"
    }

}

data class Person(
        var name: String
)
