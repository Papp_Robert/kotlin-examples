package com.test.myapplication.rx

import com.test.myapplication.BaseTest
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.AsyncSubject
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import org.junit.Test

class SubjectExampleTest : BaseTest() {


    @Test
    fun `Publish subject example`() {
        val source = PublishSubject.create<Int>()

        source.subscribe(getFirstObserver())
        source.onNext(1)
        source.onNext(2)
        source.onNext(3)

        source.subscribe(getSecondObserver())
        source.onNext(4)

        source.onComplete()
    }

    @Test
    fun `Replay subject example`() {
        val source = ReplaySubject.create<Int>()

        source.subscribe(getFirstObserver())
        source.onNext(1)
        source.onNext(2)
        source.onNext(3)

        source.subscribe(getSecondObserver())
        source.onNext(4)

        source.onComplete()
    }

    @Test
    fun `Behavior subject example`() {
        val source = BehaviorSubject.create<Int>()

        source.subscribe(getFirstObserver())
        source.onNext(1)
        source.onNext(2)
        source.onNext(3)

        source.subscribe(getSecondObserver())
        source.onNext(4)

        source.onComplete()
    }

    @Test
    fun `Async subject example`() {
        val source = AsyncSubject.create<Int>()

        source.subscribe(getFirstObserver())
        source.onNext(1)
        source.onNext(2)
        source.onNext(3)

        source.subscribe(getSecondObserver())
        source.onNext(4)

        source.onComplete()
    }

    private fun getFirstObserver(): Observer<Int> {
        //TUDJUK mire jó itt az object? Névtelen osztály.
        return object : Observer<Int> {
            override fun onNext(value: Int) {
                println(" First onNext : value : " + value)
            }

            override fun onSubscribe(d: Disposable) {
                println(" First onSubscribe : isDisposed :" + d.isDisposed)
            }

            override fun onError(e: Throwable) {
                println(" First onError : " + e.message)
            }

            override fun onComplete() {
                println(" First onComplete")
            }
        }
    }

    private fun getSecondObserver(): Observer<Int> {
        return object : Observer<Int> {
            override fun onNext(value: Int) {
                println(" Second onNext : value : " + value)
            }

            override fun onSubscribe(d: Disposable) {
                println(" Second onSubscribe : isDisposed :" + d.isDisposed)
            }

            override fun onError(e: Throwable) {
                println(" Second onError : " + e.message)
            }

            override fun onComplete() {
                println(" Second onComplete")
            }
        }
    }
}