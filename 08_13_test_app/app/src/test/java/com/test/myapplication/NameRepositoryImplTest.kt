package com.test.myapplication

import com.test.myapplication.rx.DB
import com.test.myapplication.rx.NameRepositoryImpl
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import java.lang.NullPointerException

class NameRepositoryImplTest : BaseTest() {

    @Mock
    lateinit var db: DB

    @Test
    fun `GIVEN When the db contains three names WHEN the get names is called THEN these three values are passed`() {
        createSut().apply {

            //amikor meghívódik a getnames from db függvény akkor add vissza ezt a három értéket
            //GIVEN
            Mockito.`when`(db.getNamesFromDb()).thenReturn(
                Observable.just(listOf("Béla", "János", "Géza"))
            )
            //WHEN
            val testObservable = getNames().test()

            //THEN
            Assert.assertEquals(
                listOf("Béla", "János", "Géza"),
                testObservable.values().first()
            )

        }
    }

    @Test
    fun `GIVEN When the returns an error WHEN the get names is called THEN the error is passed`() {
        createSut().apply {

            //amikor meghívódik a getnames from db függvény akkor add vissza ezt a három értéket
            //GIVEN
            Mockito.`when`(db.getNamesFromDb()).thenReturn(
                Observable.error(NullPointerException())
            )
            //WHEN
            val testObservable = getNames().test()

            //THEN
            testObservable.assertError(NullPointerException::class.java)
        }
    }


    private fun createSut() = NameRepositoryImpl(db)
}