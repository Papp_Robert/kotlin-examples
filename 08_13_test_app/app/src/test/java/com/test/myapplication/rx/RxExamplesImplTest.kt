package com.test.myapplication.rx

import com.test.myapplication.BaseTest
import io.reactivex.Observable
import io.reactivex.Observable.just
import io.reactivex.rxkotlin.subscribeBy
import org.junit.Assert
import org.junit.Test
import java.lang.Exception
import java.lang.NullPointerException
import java.lang.RuntimeException

class RxExamplesImplTest : BaseTest() {

    @Test
    fun `WHEN the value contains one element with named alma then the return value contains one element with 4`() {
        createSut().apply {
            val returnedValue = getLengthOfString(listOf("alma"))

            Assert.assertEquals(listOf(4), returnedValue)
        }
    }

    @Test
    fun `When the value contains two elements with named alma, barack then two elements is returned with 4 and 6`() {
        createSut().apply {
            //TODO: Subjecttel kicsit bonyolítani

            val returnedValue = getLengthOfStringRx(just(listOf("alma", "barack"))).test()

            //Assert.assertEquals(just(listOf(4, 6)), returnedValue)

            Assert.assertEquals(listOf(4, 6), returnedValue.values().first())
        }
    }

    @Test
    fun `WHEN the value contains one element with labda than no values are returned`() {
        createSut().apply {
            val testObserver = getLenghtOfStringIfItsBiggerThanFiveRx(just(listOf("labda"))).test()

            Assert.assertTrue(testObserver.values().first().count() == 0)
        }
    }


    @Test
    fun `GIVEN values are available from one to ten WHEN ValueFilteringUnderFive is called THEN the Observable contains the correct values`(){
        createSut().apply {
            val expectedValues = listOf(6,7,8,9,10)
            val original = Observable.just(9,6,7,5,3,1,2,8,10,4)

            valueFilteringUnderFive(original).test().assertNoErrors().assertValueSet(expectedValues)
        }
    }

    @Test
    fun `GIVEN exception WHEN valueFilteringUnderFive is called THEN exception returns`(){
        createSut().apply {
            valueFilteringUnderFive(Observable.error(RuntimeException("Error"))).test().assertError(NullPointerException::class.java)

        }


    }

    private fun createSut() = RxExamplesImpl()
}