package com.test.myapplication.rx

import com.test.myapplication.BaseTest
import io.reactivex.Observable
import io.reactivex.internal.operators.flowable.FlowableBlockingSubscribe.subscribe
import org.junit.Test
import java.util.concurrent.TimeUnit

class TransformOperatorsExample : BaseTest() {


    @Test
    fun flatMapVsConcatMap() {
        System.out.println("******** Using flatMap() *********");

        Observable.range(1, 15)
            .flatMap { item ->
                Observable.just(item).delay(1, TimeUnit.MILLISECONDS)
            }
            .subscribe { x ->
                System.out.print(x)
                System.out.print(' ')
            }

        Thread.sleep(100)

        System.out.println("\n******** Using concatMap() *********");

        Observable.range(1, 15)
            .concatMap { item -> Observable.just(item).delay(1, TimeUnit.MILLISECONDS) }
            .subscribe { x ->
                System.out.print(x)
                System.out.print(' ')
            }

        Thread.sleep(100)
    }
}