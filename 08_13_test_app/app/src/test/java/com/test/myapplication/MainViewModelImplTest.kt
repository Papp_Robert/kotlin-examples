package com.test.myapplication

import android.content.Context
import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mock

class MainViewModelImplTest : BaseTest() {

    @Mock
    lateinit var context: Context

    @Test
    fun `When adding four and one than its five`() {
        createSut().apply {
            Assert.assertEquals(5, addTwoNumbers(4, 1))
        }
    }

    @Test
    fun `When adding seven and six than its thirteen`() {
        createSut().apply {
            Assert.assertEquals(13, addTwoNumbers(6, 7))
        }
    }

    @Test(expected = NegativeNumberException::class)
    fun `When adding negative number to positive number exception is thrown`() {
        createSut().apply {
            addTwoNumbers(-5, 4)
        }
    }


    private fun createSut() = MainViewModelImpl(context)
}
