package com.test.myapplication.standard_functions

import com.test.myapplication.BaseMockkTest
import com.test.myapplication.rx.NameRepository
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Assert
import org.junit.Test

class StandardFunctionViewModelImplTest : BaseMockkTest() {

    @MockK
    lateinit var nameRepository: NameRepository

    private fun withSut(action: StandardFunctionViewModelImpl.() -> Unit) =
        StandardFunctionViewModelImpl(nameRepository).apply(action)

    @Test
    fun `GIVEN name is presented with id in the DB WHEN get filtered name is called THEN the value is returned`() {
        val id = "id"
        val returnedValue = "COVID-19"
        every {
            nameRepository.getName(id)
        } returns listOf(returnedValue)

        every {
            nameRepository.getName("")
        } returns emptyList()


        withSut {
            Assert.assertEquals(
                returnedValue, getFilteredNames(id)
            )
        }

    }



    @Test
    fun `GIVEN name is presented without id in the DB WHEN get filtered name is called THEN the value is returned`() {
        val id = "id"
        val returnedValue = "COVID-19"
        every {
            nameRepository.getName(id)
        } returns emptyList()

        every {
            nameRepository.getName("")
        } returns listOf(returnedValue)


        withSut {
            Assert.assertEquals(
                returnedValue, getFilteredNames(id)
            )
        }

    }

}