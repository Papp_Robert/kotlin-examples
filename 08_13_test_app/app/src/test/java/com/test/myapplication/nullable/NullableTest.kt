package com.test.myapplication.nullable

import org.junit.Assert
import org.junit.Test

class NullableTest {





    private fun withSut(action: NullableExample.() -> Unit) = NullableExample().apply(action)


    @Test
    fun `GIVEN Person has more dog WHEN getFavoriteDogName is called THEN return empty String`(){

        val firstPerson = Person(
            listOf(Dog("Lessie"), Dog("Morzsi"))
        )

        withSut {
            Assert.assertEquals("", getFavoriteDogName(firstPerson))
        }
    }


    @Test
    fun `GIVEN Person has one dog WHEN getFavoriteDogName is called THEN return this dog's name`(){

        val secondPerson = Person(listOf(Dog("Bogár")))

        withSut {
            Assert.assertEquals("Bogár", getFavoriteDogName(secondPerson))
        }
    }


    @Test
    fun `GIVEN Person has zero dog WHEN getFavoriteDogName is called THEN return empty String`(){

        val thirdPerson = Person(emptyList())

        withSut {
            Assert.assertEquals("", getFavoriteDogName(thirdPerson))
        }
    }


}