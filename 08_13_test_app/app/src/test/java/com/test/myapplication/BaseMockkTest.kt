package com.test.myapplication

import io.mockk.MockKAnnotations
import org.junit.Before

open class BaseMockkTest {

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
    }

}