package com.test.myapplication

import com.test.myapplication.kotlinPractices.BankAccount
import org.junit.Test

class BankAccountTest : BaseTest() {

    @Test
    fun testWithdraw() {
        withSut {
            println(this.balance)

            deposit(100.0)

            print(this.balance)

            withdraw(50.0)
            println(balance)
        }
    }

    @Test
    fun testMutableProblem() {
        withSut {
            mutableProblem()
        }
    }

    @Test
    fun testLock() {
        withSut {
            lockExample()
        }
    }

    private fun withSut(action: BankAccount.() -> Unit) = BankAccount().apply(action)
}