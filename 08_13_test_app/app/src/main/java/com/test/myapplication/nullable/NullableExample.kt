package com.test.myapplication.nullable

class NullableExample {

    fun getAnimalName(animal: Animal): String {
        return animal.dog?.name?.let {
            //A kérdőjel ponton túl csak akkor jut el a program ha nem null a name
            it
        } ?: ""
    }



    fun getFavoriteDogName(person: Person) : String{

        return person.dogs.takeIf {
            it.count() == 1
        }?.let {
            it.first().name
        } ?: ""


        /*if(person.dogs.count() == 1)
        {
            return person.dogs.first().name?.let {
                it
            } ?: ""
        }
        return ""*/
    }
}

data class Animal(val dog: Dog?){
    fun isMammal() = true
}

data class Dog(val name: String?)

data class Person(val dogs: List<Dog>)