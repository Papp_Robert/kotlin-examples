package com.test.myapplication.factory

interface IGoalKeeper {
    fun goalKeeperName(): String
}

class EnglandTeam : IGoalKeeper {
    override fun goalKeeperName(): String = "Pickford"
}

class SpanishTeam : IGoalKeeper {
    override fun goalKeeperName(): String = "De Gea" //Kepa?
}

enum class Nationality {
    English, Spanish
}

object FactoryExample {
    fun goalKeeperForNationality(nationality: Nationality): IGoalKeeper {
        return when (nationality) {
            Nationality.English -> EnglandTeam()
            Nationality.Spanish -> SpanishTeam()
        }
    }
}

