package com.test.myapplication.subjects

import com.test.myapplication.Person

class WithoutHighOrderFunction {
    fun getOlderPerson(firstPerson: Person, secondPerson: Person): Person {
        if (firstPerson.age > secondPerson.age) {
            return firstPerson
        } else {
            return secondPerson
        }
    }
}