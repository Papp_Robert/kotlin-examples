package com.test.myapplication

class HighOrderFunction {
    fun getPersonOlderThanFive(firstPerson: Person, secondPerson: Person) =
        run {
            if (firstPerson.age > secondPerson.age) firstPerson else secondPerson
        }

    fun addToAge(firstPerson: Person) =
        with(firstPerson) {
            if (firstPerson.age > 12) 12 else 24
        }.also {
            
        }

    fun addToAgeWithRun(firstPerson: Person) =
        firstPerson.run {
            age += 1
        }

    fun addToAgeWithLet(firstPerson: Person) =
        firstPerson.let {
            it.age += 1
        }

    fun modifyPersonWithApply(firstPerson: Person) = firstPerson.apply {
        age = 12
        name = "Test"
    }.run {

    }

    fun modifyPersonWithAlso(firstPerson: Person) = firstPerson.also {
        it.age = 12
        it.name = "Test"
    }

}

data class Person(var name: String, var age: Int)

