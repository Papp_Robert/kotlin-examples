package com.test.myapplication.kotlinPractices

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.Exception
import kotlin.concurrent.thread

class BankAccount {
    var balance = 0.0
        private set

    fun deposit(depositAmount: Double) {
        balance += depositAmount
    }

    @Throws(InsufficientFunds::class)
    fun withdraw(withdrawAmount: Double) {
        if (balance < withdrawAmount) {
            throw InsufficientFunds()
        }
        balance -= withdrawAmount
    }

    fun mutableProblem() {
        var num = 0
        for (i in 1..1000) {
            thread {
                Thread.sleep(10)
                num += 1
            }
        }
        Thread.sleep(5000)
        print(num)
    }

    fun lockExample() {
        val lock = Any()
        var num = 0
        for (i in 1..1000) {
            thread {
                Thread.sleep(10)
                synchronized(lock) {
                    num += 1
                }
            }
        }
        Thread.sleep(1000)
        print(num)
    }

    suspend fun coroutineExapmle() {
        var num = 0
        coroutineScope {
            for (i in 1..1000) {
                launch {
                    delay(10)
                    num += 1
                }
            }
        }
        print(num)
    }

    fun valVsVar() {
        val name: String? = "Márton"
        val surname: String = "Braun"

        val fullName2: String? = name?.let { "$it $surname" }
    }

    fun sieveOfEratosthenes(){
        var numbers = (2..100).toList()
        val primes = mutableListOf<Int>()

        while(numbers.isNotEmpty()){
            val prime = numbers.first()
            primes.add(prime)
            numbers = numbers.filter { it % prime != 0 }
        }
    }

    fun sieveOfEratosthenesBetter(){
        var numbers = (2..100).toList()
        val primes = mutableListOf<Int>()

        while(numbers.isNotEmpty()){
            val prime = numbers.first()
            primes.add(prime)
            numbers = numbers.filter { it % prime != 0 }
        }
    }
}

class InsufficientFunds : Exception()