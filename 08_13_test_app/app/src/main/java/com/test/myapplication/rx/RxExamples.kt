package com.test.myapplication.rx

import io.reactivex.Observable

interface RxExamples {
    fun getLengthOfString(value: List<String>): List<Int>
    fun getLengthOfStringRx(rxValues: Observable<List<String>>): Observable<List<Int>>
    fun getLenghtOfStringIfItsBiggerThanFiveRx(rxValues: Observable<List<String>>): Observable<List<Int>>
    fun valueFilteringUnderFive(originalObservable : Observable<Int>) : Observable<Int>
}

class RxExamplesImpl : RxExamples {

    override fun getLengthOfString(value: List<String>): List<Int> =
        //FOREACH-KISZEDJÜK AZ ELEMET/ HOZZÁADJUK EGY LISTÁHOZ / VISSZATÉRÜNK A LISTÁVAL
        value.map {
            it.length
        }

    override fun getLengthOfStringRx(rxValues: Observable<List<String>>): Observable<List<Int>> {
        return rxValues.map {
            it.map {
                it.length
            }
        }
    }

    override fun getLenghtOfStringIfItsBiggerThanFiveRx(rxValues: Observable<List<String>>): Observable<List<Int>> =
        rxValues
            .map {
                it.map {
                    it.length
                }
            }
            .flatMapIterable {
                it
            }
            .filter {
                it > 5
            }.toList().toObservable()

    override fun valueFilteringUnderFive(originalObservable : Observable<Int>): Observable<Int> {

        return originalObservable.filter {
            it>5
        }.sorted()

    }
}