package com.test.myapplication

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.test.myapplication.factory.EnglandTeam
import com.test.myapplication.factory.Nationality
import com.test.myapplication.factory.SpanishTeam

class MainViewModelImpl(context: Context) : MainViewModel, ViewModel() {

    /*init {
        aNumber.set("1231231231r5csdxf")
    }*/

    override val aNumber: ObservableField<String> = ObservableField()
    override val bNumber: ObservableField<String> = ObservableField()

    override fun addTwoNumbers(firstNumber: Int, secondNumber: Int): Int {
        if ((firstNumber < 0).or(secondNumber < 0)) {
            throw NegativeNumberException()
        }
        return firstNumber + secondNumber
    }

    fun getGoalKeeperName(nationality: Nationality) {
        if (nationality == Nationality.English) {
            EnglandTeam()
        } else {
            SpanishTeam()
        }
    }
}

class NegativeNumberException : Exception()