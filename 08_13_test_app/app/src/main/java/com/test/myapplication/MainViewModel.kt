package com.test.myapplication

import androidx.databinding.ObservableField

interface MainViewModel {

    val aNumber: ObservableField<String>

    val bNumber: ObservableField<String>

    fun addTwoNumbers(firstNumber: Int, secondNumber: Int) : Int
}