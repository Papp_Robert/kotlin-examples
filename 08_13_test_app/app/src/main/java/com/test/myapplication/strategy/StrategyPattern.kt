package com.test.myapplication.strategy

abstract class Strategy {
    abstract fun print(str: String): String
}

class LowerCaseStrategy : Strategy() {
    override fun print(str: String) = str.toLowerCase()
}

class UpperCaseStrategy : Strategy() {
    override fun print(str: String) = str.toUpperCase()
}