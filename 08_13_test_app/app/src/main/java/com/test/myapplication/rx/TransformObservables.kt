package com.test.myapplication.rx

import android.util.Log
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Function
import io.reactivex.rxkotlin.subscribeBy

class TransformObservables {

    val compositeDisposable = CompositeDisposable()

    fun bufferExample() {
        compositeDisposable.add(Observable.just(1, 2, 3, 4, 5, 6).buffer(2).subscribeBy(

            onNext = { list ->
                list.forEach { number ->
                    Log.e("GYAKORLAS", number.toString())
                }
            }
        ))
    }

    fun mapExample() {
        compositeDisposable.add(
            Observable.just(4, 8, 12, 41).map {
                it % 2
            }.subscribe(
                {
                    Log.e("GYAKORLAS", it.toString())
                },
                {

                }
            )
        )

        val mapFunction = object : Function<Int, String> {
            override fun apply(t: Int): String {
                return "almafa"
            }

        }

        compositeDisposable.add(
            Observable.just(4, 8, 12, 41).map(object : Function<Int, Int> {
                override fun apply(t: Int): Int {
                    return t % 2
                }
            }
            ).subscribe(
                {
                    Log.e("GYAKORLAS", it.toString())
                },
                {

                }
            )
        )
    }

    fun flatmapExample2(){
        val obs = Observable.just(1,2,3)
        val animalsOBs = Observable.just("kutya", "macska")

        animalsOBs.flatMap {

            obs
        }.map {

        }

    }

    fun flatMapExample() {
        originalObservable().flatMap {
            getModifiedObservable(it)
        }.subscribeBy(
            onNext = {
                Log.e("GYAKORLAS", it.toString())
            }
        )
    }

    fun switchMapExample() {
        originalObservable().switchMap {
            getModifiedObservable(it)
        }.subscribeBy(
            onNext = {
                Log.e("GYAKORLAS", it.toString())
            }
        )
    }

    fun concatMapExample() {
        originalObservable().concatMap {
            getModifiedObservable(it)
        }.subscribeBy(
            onNext = {
                Log.e("GYAKORLAS", it.toString())
            }
        )
    }

    fun originalObservable(): Observable<Int> {
        return Observable.fromArray(1, 2, 3, 4, 5, 6)
    }

    fun getModifiedObservable(num: Int) =
        Observable.create<Int> { emitter ->
            emitter.onNext(num * 2)
            emitter.onComplete()
        }
}