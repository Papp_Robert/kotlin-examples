package com.test.myapplication.rx

import io.reactivex.Observable

interface NameRepository {
    fun getNames(): Observable<List<String>>
    fun getName(id: String): List<String>
}

class NameRepositoryImpl(private val db: DB) : NameRepository {
    override fun getNames(): Observable<List<String>> {

        return db.getNamesFromDb()
    }

    override fun getName(id: String): List<String> {
        TODO("Not yet implemented")
    }
}

class DB() {
    fun getNamesFromDb() =
        Observable.just(listOf("Béla", "János", "Géza"))

    fun getNamesFromDb2() = Observable.create<List<String>> { emitter ->
        emitter.onNext(listOf("Béla"))
        emitter.onNext(listOf("János"))
        emitter.onNext(listOf("Géza"))
        //asasd
        //asdasd

        emitter.onNext(listOf("Juli"))
        emitter.onComplete()

        "juli".almafa()
    }
}

fun String.almafa() = this.plus("almafa")