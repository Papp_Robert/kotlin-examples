package com.test.myapplication.facade

class FacadePattern {
    init {
        val userRepository = UserRepository()
        val user = User("dbacinski")
        userRepository.save(user)
        val resultUser = userRepository.findFirst()
        println("Found stored user: $resultUser")
    }
}

class UserSharedPrefStore(val filePath: String) {
    init {
        println("Reading data from shared pref: $filePath")
    }

    val store = HashMap<String, String>()

    fun store(key: String, payload: String) {
        store.put(key, payload)
    }

    fun read(key: String): String = store[key] ?: ""

    fun commit() = println("Storing cached data: $store to shared pref: $filePath")
}

data class User(val login: String)

//Facade:
class UserRepository {
    val systemPreferences = UserSharedPrefStore("/data/default.prefs")

    fun save(user: User) {
        systemPreferences.store("USER_KEY", user.login)
        systemPreferences.commit()
    }

    fun findFirst(): User = User(systemPreferences.read("USER_KEY"))
}