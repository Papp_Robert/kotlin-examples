enum class CarType {
    MICRO, MINI, LUXURY
}

enum class Location {
    DEFAULT, USA, INDIA
}

abstract class Car(open val model: CarType, open val location: Location) {
    override fun toString(): String {
        return "CarModel - $model located in $location"
    }
}

data class LuxuryCar(override val location: Location) : Car(CarType.LUXURY, location) {
}

data class MicroCar(override val location: Location) : Car(CarType.MICRO, location) {
}

data class MiniCar(override val location: Location) : Car(CarType.MINI, location) {
}

class IndiaCarFactory {
    fun buildCar(model: CarType): Car {
        return when (model) {
            CarType.MICRO -> MicroCar(Location.INDIA)

            CarType.MINI -> MiniCar(Location.INDIA)

            CarType.LUXURY -> LuxuryCar(Location.INDIA)
        }
    }
}

class USACarFactory {
    fun buildCar(model: CarType): Car {
        return when (model) {
            CarType.MICRO -> MicroCar(Location.USA)

            CarType.MINI -> MiniCar(Location.USA)

            CarType.LUXURY -> LuxuryCar(Location.USA)
        }
    }
}


class CarFactory {
    fun buildCar(type: CarType): Car? {
        val location = Location.INDIA

        return when (location) {
            Location.USA -> USACarFactory().buildCar(type)

            Location.INDIA -> IndiaCarFactory().buildCar(type)

            else -> buildCar(type)
        }
    }
}

//println(CarFactory().buildCar(CarType.MICRO))
//println(CarFactory().buildCar(CarType.MINI))
//println(CarFactory().buildCar(CarType.LUXURY))