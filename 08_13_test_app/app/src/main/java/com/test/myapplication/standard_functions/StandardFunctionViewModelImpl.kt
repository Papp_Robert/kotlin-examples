package com.test.myapplication.standard_functions

import com.test.myapplication.rx.NameRepository

class StandardFunctionViewModelImpl(private val nameRepository: NameRepository) {

    fun getFilteredNames(id: String): String {


        nameRepository.getName(id).apply {
            return if(this.isEmpty()){
                nameRepository.getName("").first()
            }
            else this.first()
        }

        /*Mi van ha nem stringet hanem Observable<String>-et adna vissza?*/
        /*Mi van ha mindkettő üres? (crash, amit várunk)*/

        /*val returnValuewithFilledId = nameRepository.getName(id)

        if(returnValuewithFilledId.isEmpty()){
            return nameRepository.getName("").first()
        }
        else{
            return returnValuewithFilledId.first()
        }*/

    }

}