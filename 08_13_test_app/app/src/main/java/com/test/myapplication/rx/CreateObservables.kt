package com.test.myapplication.rx

import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit

class CreateObservables {

    fun waysToCreateObservables() {
        val obs = Observable.create<String> { emitter ->
            emitter.onNext("kutyus")
            emitter.onNext("kiskutyus")
            emitter.onComplete()
        }

        val disp = obs.subscribeBy (
            onNext = {

            },
            onComplete = {

            },
            onError = {

            }
        )

        val obs2 = Observable.fromArray("kutyus", "kiskutyus")

        val obs3 = Observable.interval(1, TimeUnit.SECONDS)

        val obs4 = Observable.just("kutyus", "kiskutyus")

        val obs5 = Observable.range(2, 10)

        val obs6 = Observable.range(2, 10).repeat(2)

        val obs7 = Observable.timer(1, TimeUnit.SECONDS)


    }
}